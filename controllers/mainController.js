/**
 * Method: GET
 * URI: /
 * */

exports.home = function(req, res, next) {
    res.render('index', {title: 'Clear for take off'});
    next();
};